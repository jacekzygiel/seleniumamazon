package com.zygiel.cukes;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {
                "pretty", "html:target/cucumberHtmlReport",
                "html:target/cucumberHtmlReport",
                "pretty:target/cucumber-json-report.json"
        },
        features = "classpath:features",
        glue = "com.zygiel.stepdefs"
)

public class RunCukeTest {
}
